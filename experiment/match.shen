(define to-string
[] -> ""
X -> (eval [@s | X]))

(defcc <users>
<domain> ($ "/users/") <notslash> := [(to-string <domain>) users (to-string <notslash>)];
)

(defcc <domain> <notslash> "/" "/" <notslash+>;)
(defcc <notslash+> <notslash>;)
(defcc <notslash>
X <notslash> := [X | <notslash>] where (not (= X "/")) ;
X := [X] where (not (= X "/"));)

((compile (fn <domain>)) (explode "https://hai.z0ne.social"))
((compile (fn <users>)) (explode "https://hai.z0ne.social/users/9zyjlcpz5j7j0ct6"))

since_id
until_id

