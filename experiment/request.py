import aiohttp
import asyncio
import json
import time
import hashlib
import base64
from urllib.parse import urlparse
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives.serialization import load_pem_private_key

class ActivityPubClient:
    def __init__(self, actor_url, private_key=None, key_id=None):
        self.actor_url = actor_url
        self.private_key = private_key
        self.key_id = key_id
        self.session = None
        
    def _sign_request(self, method, url, headers, body=None):
        """Create HTTP Signature for request"""
        if not self.private_key or not self.key_id:
            return headers
            
        # Create the signature string
        date = headers.get('Date', time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime()))
        host = urlparse(url).netloc
        
        # Create the signed headers list
        signed_headers = ['(request-target)', 'host', 'date']
        if body:
            digest = base64.b64encode(hashlib.sha256(body.encode()).digest()).decode()
            headers['Digest'] = f"SHA-256={digest}"
            signed_headers.append('digest')
            
        # Create the signature string
        signature_string = []
        for h in signed_headers:
            if h == '(request-target)':
                signature_string.append(f"(request-target): {method.lower()} {urlparse(url).path}")
            else:
                signature_string.append(f"{h}: {headers[h]}")
        signature_string = "\n".join(signature_string)
        
        # Sign the string
        private_key = load_pem_private_key(self.private_key.encode(), password=None)
        signature = private_key.sign(
            signature_string.encode(),
            padding.PKCS1v15(),
            hashes.SHA256()
        )
        signature = base64.b64encode(signature).decode()
        
        # Add signature to headers
        headers['Signature'] = (
            f'keyId="{self.key_id}",'
            f'algorithm="rsa-sha256",'
            f'headers="{" ".join(signed_headers)}",'
            f'signature="{signature}"'
        )
        return headers

    async def fetch(self, url, method='GET', body=None):
        """Fetch an ActivityPub resource"""
        if not self.session:
            self.session = aiohttp.ClientSession()
            
        headers = {
            "Accept": "application/activity+json",
            "User-Agent": "ActivityPubClient/1.0",
            "Date": time.strftime("%a, %d %b %Y %H:%M:%S GMT", time.gmtime())
        }
        
        # Sign the request if we have credentials
        headers = self._sign_request(method, url, headers, body)
        
        try:
            async with self.session.request(method, url, headers=headers, data=body) as response:
                if response.status == 200:
                    data = await response.json()
                    return data
                else:
                    print(f"Error fetching {url}: {response.status}")
                    return None
        except Exception as e:
            print(f"Request failed: {e}")
            return None
            
    async def get_actor(self):
        """Fetch actor information"""
        return await self.fetch(self.actor_url)
        
    async def get_inbox(self):
        """Fetch actor's inbox"""
        actor = await self.get_actor()
        if actor and 'inbox' in actor:
            return await self.fetch(actor['inbox'])
        return None
        
    async def close(self):
        """Close the client"""
        if self.session:
            await self.session.close()

async def main():
    # Example usage
    client = ActivityPubClient("https://mastodon.social/users/Gargron")
    
    try:
        # Fetch actor profile
        actor = await client.get_actor()
        if actor:
            print(f"Fetched actor: {actor['name']}")
            
        # Fetch inbox
        inbox = await client.get_inbox()
        if inbox:
            print(f"Fetched {len(inbox['orderedItems'])} items from inbox")
            
    finally:
        await client.close()

if __name__ == "__main__":
    asyncio.run(main())

