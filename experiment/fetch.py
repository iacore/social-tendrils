import sys
import requests
import json
import shelve
from urllib.parse import urlparse

def fetch_activitypub(url):
    """Fetch an ActivityPub resource"""
    headers = {
        "Accept": "application/activity+json",
        "User-Agent": "ActivityPubClient/1.0"
    }
    
    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            return response.json()
        else:
            print(f"Error fetching {url}: {response.status_code}")
            return None
    except Exception as e:
        print(f"Request failed: {e}")
        return None

def main():
    if len(sys.argv) < 2:
        print("Usage: fetch.py <url>")
        return
        
    url = sys.argv[1]
    data = fetch_activitypub(url)
    
    if data:
        with shelve.open('requests') as db:
            db[url] = data
            print(json.dumps(data, indent=2))

if __name__ == "__main__":
    main()
