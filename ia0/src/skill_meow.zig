const std = @import("std");

pub fn do(writer: anytype) !void {
    var prng = std.rand.DefaultPrng.init(@bitCast(std.time.microTimestamp()));
    const rng = prng.random();

    const n_words = count_words(rng);
    for (0..n_words) |i| {
        const word_opts = word_mutation(rng);
        const n_syllables = count_syllables(rng);

        if (i != 0) try writer.writeByte(' ');
        for (0..n_syllables) |j| {
            const syl = all_syllables[rng.uintLessThan(usize, all_syllables.len)];
            var syl_opts = syllable_mutation(rng);
            if (word_opts.last_letter_cut and j == n_syllables - 1)
                syl_opts.last_letter_cut = true;
            try syl.print(syl_opts, writer);
        }
        if (word_opts.suffix) |suffix| try writer.writeByte(suffix);
    }

    const n_emojis = count_emojis(rng);
    for (0..n_emojis) |i| {
        const emoji = all_emojis[rng.uintLessThan(usize, all_emojis.len)];
        if (i == 0) try writer.writeByte(' ');
        try writer.writeByte(':');
        try writer.writeAll(emoji);
        try writer.writeByte(':');
    }
    // writer.write("meow");
}

/// each sentence 0-2 emojis
fn count_emojis(rng: std.rand.Random) usize {
    const threshold: f32 = rng.float(f32);
    if (threshold <= 0.5) return 0;
    if (threshold <= 0.8) return 1;
    if (threshold <= 1.0) return 2;
    unreachable;
}

/// each sentence around 4-8 maybe 10 words
fn count_words(rng: std.rand.Random) usize {
    return @intFromFloat(gamma(rng, 5, 1));
}

/// gamma distribution
/// borrowed from d3-random
/// https://github.com/d3/d3-random/blob/588790e06454c27f7dae4e7cffeadf2c783d88c3/src/gamma.js#L4
fn gamma(rng: std.rand.Random, k: f32, theta: f32) f32 {
    const math = std.math;

    if (k < 0) @panic("invalid k");
    if (k == 0) return 0;
    if (k == 1) return -math.log1p(rng.floatNorm(f32)) * theta;

    const d = (if (k < 1) (k + 1) else k) - 1 / 3;
    const c = 1 / (3 * math.sqrt(d));
    const multiplier = if (k < 1) (math.pow(f32, rng.floatNorm(f32), 1 / k)) else 1;

    var x: f32 = undefined;
    var v: f32 = undefined;
    var u: f32 = undefined;

    var _l0_first = true;
    while (_l0_first or (u >= 1 - 0.0331 * x * x * x * x and @log(u) >= 0.5 * x * x + d * (1 - v + @log(v)))) : (_l0_first = false) {
        var _l1_first = true;
        while (_l1_first or (v <= 0)) : (_l1_first = false) {
            x = rng.floatNorm(f32);
            v = 1 + c * x;
        }
        v *= v * v;
        u = 1 - rng.floatNorm(f32);
    }

    return d * v * multiplier * theta;
}

/// each word 1-3 syllables
fn count_syllables(rng: std.rand.Random) usize {
    const threshold: f32 = rng.float(f32);
    if (threshold <= 0.3) return 1;
    if (threshold <= 0.8) return 2;
    if (threshold <= 1.0) return 3;
    unreachable;
}

// random: extend mid of a random syllable
fn syllable_mutation(rng: std.rand.Random) SyllableMutation {
    return .{
        .extend_mid_by = if (rng.float(f32) < 0.75) 0 else @intFromFloat(gamma(rng, 5, 1)),
    };
}

// random: last syllable last letter cut
// random: additional letter suffix to word
fn word_mutation(rng: std.rand.Random) struct {
    last_letter_cut: bool,
    suffix: ?u8,
} {
    return .{
        .last_letter_cut = (rng.float(f32) < 0.3),
        .suffix = if (rng.float(f32) < 0.1) random_suffix(rng) else null,
    };
}

fn random_suffix(rng: std.rand.Random) u8 {
    return all_suffix[rng.uintLessThan(usize, all_suffix.len)];
}

const all_suffix = [_]u8{ 'u', 'w', 'a', 'n' };

const Syllable = struct {
    start: []const u8,
    mid: []const u8,
    end: []const u8,

    fn init(x: anytype) @This() {
        return .{
            .start = x[0],
            .mid = x[1],
            .end = x[2],
        };
    }

    fn print(this: @This(), opts: SyllableMutation, writer: anytype) !void {
        try writer.writeAll(this.start);
        for (this.mid) |char| {
            try writer.writeByte(char);
            for (0..opts.extend_mid_by) |_| {
                try writer.writeByte(char);
            }
        }
        if (opts.last_letter_cut) {
            try writer.writeAll(this.end[0 .. this.end.len - 1]);
        } else {
            try writer.writeAll(this.end);
        }
    }
};

const SyllableMutation = struct {
    extend_mid_by: usize,
    last_letter_cut: bool = false,
};

const all_syllables = [_]Syllable{
    Syllable.init(.{ "m", "eo", "w" }),
    Syllable.init(.{ "ny", "a", "n" }),
    Syllable.init(.{ "m", "e", "w" }),
    Syllable.init(.{ "p", "u", "r" }),
    // .{"m", "r", "p"},
};

const all_emojis = [_][]const u8{
    "meow3ssmirk",
    "meowamused",
    "meowastonished",
    "meowawauu",
    "meowbaka",
    "meowblush",
    "neocat",
    "neocat__w_",
    "neocat_3c",
    "neocat_angry",
    "neocat_aww",
    "neocat_baa",
    "neocat_blank",
    "neocat_blep",
    "neocat_blush",
};
