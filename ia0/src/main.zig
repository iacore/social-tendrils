test "Table Of Contents" {
    _ = .{
        main,
        .{ // http stuff
            // base manager for http requests
            api.Sender,
            // misskey-specific API
            api.SenderTricks,
        },
        .{ // commands
            // command parser
            commands.Parser,
            // do commands
            commands.Worker,
        },
    };
}

const std = @import("std");
const builtin = @import("builtin");
const curl = @import("curl");

const api = @import("api.zig");
const commands = @import("commands.zig");

pub const std_options = struct {
    pub const log_scope_levels: []const std.log.ScopeLevel = &.{
        // uncomment this if the log is too verbose
        .{ .scope = .http, .level = .info },
    };
};

pub fn main() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    try curl.globalInit();
    defer curl.globalCleanup();

    const keys = try get_api_keys(a);
    defer {
        for (keys.items) |key| a.free(key);
        keys.deinit();
    }

    var sender = api.Sender.init(a, keys.items);
    var client = sender.tricks();

    const my_user_id = blk: {
        const desc = try commands.generate_motd(a);
        defer a.free(desc);

        while (true) {
            const data_user = client.update_my_profile(.{
                .birthday = "2023-09-19",
                .description = desc,
            }) catch |err| switch (err) {
                error.OutOfMemory => return err,
                else => continue,
            };
            defer data_user.deinit();
            const user = data_user.value orelse std.debug.panic("cannot get user\nerror: {s}", .{data_user.err.?.message});
            break :blk try a.dupe(u8, user.id);
        }
    };
    defer a.free(my_user_id);

    var latestId: ?[]const u8 = null;
    defer if (latestId) |s| a.free(s);

    while (true) {
        process: {
            const data_posts = client.get_posts_that_mentioned_me(.{
                .limit = 100,
                .sinceId = latestId,
            }) catch |err| switch (err) {
                error.OutOfMemory => return err,
                else => continue,
            };
            defer data_posts.deinit();
            const posts_requests = data_posts.value orelse {
                std.log.err("cannot get user\nerror: {s}", .{data_posts.err.?.message});
                break :process;
            };

            for (posts_requests) |request| {
                std.log.info("<<< {s}: {?s}", .{ request.id, request.text });
                if (std.mem.eql(u8, my_user_id, request.user.id)) {
                    std.log.info("skip (is self)", .{});
                    // client.delete_post(.{ .noteId = request.id }) catch ...;
                    continue;
                }

                var has_command = false;
                if (request.text) |text| {
                    var parser = commands.Parser.init(text);
                    while (parser.next()) |cmd| {
                        std.log.info("command: {s}", .{cmd});
                        has_command = true;
                    }
                }

                if (has_command) {
                    var already_replied = false;

                    if (request.repliesCount > 0) { // try reply
                        const data_replies = client.get_replies(.{
                            .noteId = request.id,
                        }) catch |err| switch (err) {
                            error.OutOfMemory => return err,
                            else => continue,
                        };
                        defer data_replies.deinit();
                        const posts_all_replies = data_replies.value orelse {
                            std.log.err("cannot get replies\nerror: {s}", .{data_replies.err.?.message});
                            break :process;
                        };

                        for (posts_all_replies) |reply| {
                            if (std.mem.eql(u8, my_user_id, reply.user.id)) {
                                std.log.info("found reply by self: {s} -> {s}", .{ reply.id, request.id });
                                already_replied = true;
                                break;
                            }
                        }
                    }

                    if (!already_replied) {
                        var worker = commands.Worker.init(a, request);
                        defer worker.deinit();
                        var parser = commands.Parser.init(request.text.?);

                        var count: usize = 0;
                        while (parser.next()) |cmd| {
                            try worker.process(cmd);
                            count += 1;
                        }
                        if (count == 0) try worker.meow();

                        const data_my_reply = client.create_post(.{
                            .replyId = request.id,
                            .text = worker.output(),
                            .cw = request.cw,
                            .visibility = request.visibility,
                            .visibleUserIds = &.{request.user.id},
                        }) catch |err| switch (err) {
                            error.OutOfMemory => return err,
                            else => continue,
                        };
                        defer data_my_reply.deinit();
                        const post_reply = data_my_reply.value orelse {
                            std.log.err("cannot reply\nerror: {s}", .{data_my_reply.err.?.message});
                            break :process;
                        };

                        std.log.info("replied: {s}", .{post_reply.createdNote.id});
                    }
                }
            }

            if (posts_requests.len > 0) {
                if (latestId) |s| a.free(s);
                latestId = try a.dupe(u8, posts_requests[0].id);
            }
        }

        std.time.sleep(10 * std.time.ns_per_s);
    }
}

fn get_api_keys(a: std.mem.Allocator) !std.ArrayList([]const u8) {
    const home = std.os.getenv("HOME") orelse @panic("No $HOME????");
    const filename = try std.fmt.allocPrint(a, "{s}/.config/tendrils/testkey", .{home});
    defer a.free(filename);
    const file = try std.fs.openFileAbsolute(filename, .{});
    defer file.close();

    var keys = std.ArrayList([]const u8).init(a);

    while (true) {
        const line = (try file.reader().readUntilDelimiterOrEofAlloc(a, '\n', 36)) orelse break;
        try keys.append(line);
    }

    return keys;
}
