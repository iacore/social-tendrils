const std = @import("std");
const curl = @import("curl");
const Timestamp = @import("Timestamp");

/// I send Misskey idiom requests
///
/// A word about []Note: newest notes are always first in array
pub const SenderTricks = struct {
    sender: *Sender,

    pub const Unit = struct {};
    pub const User = struct {
        id: []const u8,
        name: ?[]const u8,
        username: []const u8,
        host: ?[]const u8,
    };
    pub const Note = struct {
        id: []const u8,
        createdAt: Timestamp,
        user: User,
        text: ?[]const u8,
        cw: ?[]const u8,
        repliesCount: u64,
        visibility: Visibility,
    };
    pub const Visibility = enum {
        public,
        home,
        followers,
        specified,
    };

    // pub fn me(this: @This()) !Response(User) {
    //     return this.sender.post("i", Unit{}, User);
    // }

    /// can update profile, also gets user id
    pub fn update_my_profile(
        this: @This(),
        opts: Opts_my_profile,
    ) !Response(User) {
        return this.sender.post("i/update", opts, User);
    }
    pub const Opts_my_profile = struct {
        name: ?[]const u8 = null,
        description: ?[]const u8 = null,
        location: ?[]const u8 = null,
        birthday: ?[]const u8 = null, // ^([0-9]{4})-([0-9]{2})-([0-9]{2})$
        isBot: bool = true,
    };

    pub fn get_post(this: @This(), opts: Opts_gcrl) !Response(Note) {
        return this.sender.post("notes/show", opts, Note);
    }
    pub const Opts_gcrl = struct {
        noteId: []const u8,
    };

    /// get posts where the authenticated user is mentioned
    pub fn get_posts_that_mentioned_me(this: @This(), opts: Opts_abrs) !Response([]Note) {
        return this.sender.post("notes/mentions", opts, []Note);
    }
    pub const Opts_abrs = struct {
        limit: ?u64 = null, // 1 to 100
        sinceId: ?[]const u8 = null,
        untilId: ?[]const u8 = null,
        sinceDate: ?u64 = null, // ms since epoch
        untilDate: ?u64 = null, // ms since epoch
        // includeReplies: ?bool = null, // not working
    };

    /// get posts some user has posted
    pub fn get_posts_by_user(this: @This(), opts: Opts_casr) !Response([]Note) {
        return this.sender.post("users/notes", opts, []Note);
    }
    pub const Opts_casr = struct {
        userId: []const u8,
        limit: ?u64 = null, // 1 to 100
        sinceId: ?[]const u8 = null,
        untilId: ?[]const u8 = null,
        sinceDate: ?u64 = null, // ms since epoch
        untilDate: ?u64 = null, // ms since epoch
        includeReplies: ?bool = null,
    };

    pub fn delete_post(this: @This(), opts: Opts_abro) !void {
        const response = try this.sender.post("notes/delete", opts, void);
        response.deinit();
    }
    pub const Opts_abro = struct {
        noteId: []const u8,
    };

    pub fn get_replies(this: @This(), opts: Opts_axbl) !Response([]Note) {
        return this.sender.post("notes/replies", opts, []Note);
    }
    pub const Opts_axbl = struct {
        noteId: []const u8,
        limit: ?u64 = null, // 1 to 100
        sinceId: ?[]const u8 = null,
        untilId: ?[]const u8 = null,
        sinceDate: ?u64 = null, // ms since epoch
        untilDate: ?u64 = null, // ms since epoch
    };

    pub const NoteCreated = struct { createdNote: Note };
    pub fn create_post(this: @This(), opts: Opts_rbrl) !Response(NoteCreated) {
        return this.sender.post("notes/create", opts, NoteCreated);
    }
    pub const Opts_rbrl = struct {
        text: ?[]const u8 = null,
        cw: ?[]const u8 = null,
        visibility: Visibility,
        visibleUserIds: []const []const u8 = &.{},
        replyId: ?[]const u8 = null,
    };
};

/// I send HTTP requests
pub const Sender = struct {
    // borrowed
    keys: []const []const u8,

    a: std.mem.Allocator,
    key_id: usize = 0,

    const log = std.log.scoped(.http);

    pub fn init(a: std.mem.Allocator, keys: []const []const u8) @This() {
        return .{
            .a = a,
            .keys = keys,
        };
    }
    pub fn tricks(this: *@This()) SenderTricks {
        return SenderTricks{ .sender = this };
    }

    pub fn post(this: *@This(), comptime url: []const u8, input: anytype, comptime OutType: type) !Response(OutType) {
        const bearer = try std.fmt.allocPrintZ(this.a, "Authorization: Bearer {s}", .{this.keys[this.key_id]});
        defer this.a.free(bearer);
        if (this.keys.len != 1) { // rotate key
            log.debug("Using key {}", .{this.key_id});
            this.key_id = @mod(this.key_id + 1, this.keys.len);
        }

        var headers = curl.HeaderList.init();
        defer headers.freeAll();
        try headers.append("Content-Type: application/json");
        try headers.append("Accept: application/json");
        try headers.append(bearer);

        const client = try curl.Easy.init();
        defer client.cleanup();

        try client.setPost();
        try client.setUrl("https://snug.moe/api/" ++ url);
        try client.setHeaders(headers);

        const up = try std.json.stringifyAlloc(this.a, input, .{ .emit_null_optional_fields = false });
        defer this.a.free(up);
        log.debug("url={s}  up=\n{s}", .{ url, up });
        var up_stream = std.io.fixedBufferStream(up);
        try client.setReadData(@ptrCast(&up_stream));
        try client.setReadFn(curl.readFromFbs(@TypeOf(up_stream)));

        var down = std.ArrayList(u8).init(this.a);
        errdefer down.deinit();
        try client.setWriteData(@ptrCast(&down));
        try client.setWriteFn(curl.writeToFifo(@TypeOf(down)));

        try client.perform();

        const code = client.getResponseCode() catch unreachable;

        log.debug("code={} down=\n{s}", .{ code, down.items });

        if (@divFloor(code, 100) != 2) {
            const parsed = try std.json.parseFromSlice(Opts_error, this.a, down.items, .{
                .ignore_unknown_fields = true,
            });
            return Response(OutType){
                .code = code,
                .raw = down,
                .arena = parsed.arena,
                .value = null,
                .err = parsed.value.@"error",
            };
        }

        if (OutType == void) {
            return Response(OutType){
                .code = code,
                .raw = down,
                .arena = null,
                .value = void{},
                .err = null,
            };
        }

        const parsed = try std.json.parseFromSlice(OutType, this.a, down.items, .{
            .ignore_unknown_fields = true,
        });
        return Response(OutType){
            .code = code,
            .raw = down,
            .arena = parsed.arena,
            .value = parsed.value,
            .err = null,
        };
    }
};

pub const Opts_error = struct {
    @"error": MisskeyError,
};
pub const MisskeyError = struct {
    // example
    // {
    //   "error": {
    //     "message": "Invalid param.",
    //     "code": "INVALID_PARAM",
    //     "id": "3d81ceae-475f-4600-b2a8-2bc116157532",
    //     "kind": "client",
    //     "info": {
    //       "param": "#/required",
    //       "reason": "must have required property 'userId'"
    //     }
    //   }
    // }
    message: []const u8,
    code: []const u8,
    id: []const u8,
    kind: ?[]const u8 = null, // not in spec
    info: ?std.json.Value = null, // not in spec
};

/// I contain raw JSON bytes (.raw) and its parsed value (.value)
/// I own both
pub fn Response(comptime T: type) type {
    return struct {
        /// http_code
        code: isize,
        /// response body
        raw: std.ArrayList(u8),
        arena: ?*std.heap.ArenaAllocator,
        /// parsed response body (if not error)
        value: ?T,
        /// parsed response body (if error)
        err: ?MisskeyError,

        pub fn deinit(this: @This()) void {
            this.raw.deinit();
            if (this.arena) |arena| {
                const allocator = arena.child_allocator;
                arena.deinit();
                allocator.destroy(arena);
            }
        }
    };
}
