const std = @import("std");
const t = std.testing;
const DateTime = @import("time").DateTime;

pub const Parser = struct {
    s: []const u8,
    i: usize = 0,
    /// extra error message. not necessary to check
    anomaly: ?Anomaly = null,

    pub const Anomaly = enum {
        unclosed_left_bracket,
        extraneous_right_bracket,
    };

    pub fn init(s: []const u8) @This() {
        return .{ .s = s };
    }

    pub fn next(this: *@This()) ?[]const u8 {
        var depth: usize = 0;
        var start: usize = undefined;
        while (this.i < this.s.len) : (this.i += 1) {
            switch (this.s[this.i]) {
                '[' => {
                    if (depth == 0) start = this.i + 1;
                    depth += 1;
                },
                ']' => {
                    if (depth == 0) {
                        this.anomaly = .extraneous_right_bracket;
                        return null;
                    }
                    depth -= 1;
                    if (depth == 0) return this.s[start..this.i];
                },
                else => {},
            }
        }
        if (depth > 0) this.anomaly = .unclosed_left_bracket;
        return null;
    }
};

test "Parser > basic" {
    var parser = Parser.init("[help]");
    try t.expectEqualStrings("help", parser.next() orelse return error.TestUnexpectedResult);
}
test "Parser > basic 2" {
    var parser = Parser.init("[a b]");
    try t.expectEqualStrings("a b", parser.next() orelse return error.TestUnexpectedResult);
}
test "Parser > nested 1" {
    var parser = Parser.init("[[nested] commands]");
    try t.expectEqualStrings("[nested] commands", parser.next() orelse return error.TestUnexpectedResult);
}
test "Parser > nested 2" {
    var parser = Parser.init("[commands [nested]]");
    try t.expectEqualStrings("commands [nested]", parser.next() orelse return error.TestUnexpectedResult);
}

pub fn generate_motd(a: std.mem.Allocator) ![]const u8 {
    const now = DateTime.now();
    return std.fmt.allocPrint(a,
        \\oldest offspring of @iatendril's [misskey API library](https://git.envs.net/iacore/social-tendrils/src/branch/main/daemon-pairing)
        \\
        \\do not have a name yet.
        \\
        \\last seen alive at {YYYY-MM-DD kk}:{0mm} UTC
        \\
        \\Please say [hi] to me!
    , .{now});
}

const Note = @import("api.zig").SenderTricks.Note;

pub const Worker = struct {
    buf: std.ArrayList(u8),
    note: Note,

    pub fn init(a: std.mem.Allocator, note: Note) @This() {
        return .{ .buf = std.ArrayList(u8).init(a), .note = note };
    }
    pub fn reset(this: *@This()) void {
        this.buf.clearAndFree();
    }
    pub fn deinit(this: @This()) void {
        this.buf.deinit();
    }
    pub fn output(this: @This()) []const u8 {
        return this.buf.items;
    }
    pub fn meow(this: *@This()) !void {
        const writer = this.buf.writer();
        try @import("skill_meow.zig").do(writer);
    }
    pub fn process(this: *@This(), command_raw: []const u8) !void {
        const eql = std.mem.eql;
        const writer = this.buf.writer();
        const command = std.mem.trim(u8, command_raw, " ");

        if (command.len == 0) {
            try writer.writeAll(
                \\:neocat_confused::neocat_confused::neocat_confused:
            );
        } else if (eql(u8, command, "hi") or eql(u8, command, "help")) {
            try writer.writeAll(
                \\Here's what I can do.
                \\
                \\```
                \\[hi]
                // \\[hello]               Greetings!
                \\[help]                Do you need help in general?
                \\[echo]                Repeat after you
                // \\[help <command>]      You need help for a command?
                // \\[name "name here"]    Name me!
                \\[meow]                :meowaww:
                // \\[pairme]    the pairing program
                \\```
            );
        } else if (eql(u8, command, "echo")) {
            if (this.note.text) |text| try writer.writeAll(text);
        } else if (eql(u8, command, "meow")) {
            try this.meow();
        } else {
            try writer.writeAll(
                \\:neocat_confused:
            );
        }
    }
};

test "Worker > sanity test" {
    // t.log_level = .info;

    var worker = Worker.init(t.allocator, undefined);
    defer worker.deinit();

    for ([_][]const u8{ "hi", "help", "meow" }) |command| {
        std.log.info("Testing command: {s}", .{command});
        worker.reset();
        try t.expect(worker.output().len == 0);
        try worker.process(command);
        try t.expect(worker.output().len != 0);
        std.log.info("Output:\n{s}", .{worker.output()});
        try t.expect(std.mem.indexOf(u8, worker.output(), "_confused") == null);
    }
}

// todo: how to make this not cached by `zig build test`?
test "Worker > fuzz [meow]" {
    var worker = Worker.init(t.allocator, undefined);
    defer worker.deinit();

    for (0..1_000) |_| {
        worker.reset();
        try worker.process("meow");
    }
}

test "Worker > [echo]" {
    const test_input = "[echo] Please repeat my words!";
    var note: Note = undefined;
    note.text = test_input;
    var worker = Worker.init(t.allocator, note);
    defer worker.deinit();
    try worker.process("echo");
    try t.expectEqualStrings(test_input, worker.output());
}
