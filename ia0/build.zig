const std = @import("std");

var dep_timestamp: *std.Build.Dependency = undefined;

pub fn link(exe: *std.Build.LibExeObjStep) void {
    exe.addModule("Timestamp", dep_timestamp.module("Timestamp"));
    exe.addModule("mecha", dep_timestamp.module("mecha"));
    exe.addModule("time", dep_timestamp.module("time"));
}

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});
    const optimize = b.standardOptimizeOption(.{});

    dep_timestamp = b.anonymousDependency("lib/timestamp", @import("lib/timestamp/build.zig"), .{});

    const exe = b.addExecutable(.{
        .name = "ia0",

        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });

    link(exe);

    exe.linkLibC();
    exe.linkSystemLibrary("curl");
    exe.addAnonymousModule("curl", .{
        .source_file = .{ .path = "../gui/lib/zig-libcurl/src/main.zig" },
    });

    b.installArtifact(exe);

    const run_cmd = b.addRunArtifact(exe);

    run_cmd.step.dependOn(b.getInstallStep());

    if (b.args) |args| {
        run_cmd.addArgs(args);
    }

    const run_step = b.step("run", "Run the app");
    run_step.dependOn(&run_cmd.step);

    const unit_tests = b.addTest(.{
        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    link(unit_tests);

    const run_unit_tests = b.addRunArtifact(unit_tests);

    const test_step = b.step("test", "Run unit tests");
    test_step.dependOn(&run_unit_tests.step);
}
