## The Logic
The pairing process is stateless. I don't store anything locally.

== time intervals
R: refresh interval, like 5 minutes
X: "recent" time period, like 24 hours
N: some smaller time period, like 2 hours. ensures that no race condition occurs

- every R, fetch messages more recent than X and detect keyword periodically
- send message only mentioning the two people (if haven't already sent); this acts as rate-limiting as well
- delete all sent messages older than X + N

maybe bad design:
trusting the server will keep time accurately

## Todo

Behavior:
- [x] extract commands
- [x] parse/process commands

- [X] error handling (json returns {error: ...})
- [ ] find posts across pagination

api used:
- [x] fetch mentions
- [x] fetch posts sent (by me)
- [x] delete posts sent
- [x] send post with same visibility, including message post

## API Key

Create an API key with at least those permissions

- View your account information
- Edit your account information
- Compose or delete posts

Then just store the key at `~/.config/tendrils/testkey`. You can put one key per line (if you have multiple keys).

## Deploy

After you have the API key file, simply copy `zig-out/bin/xxxx` to anywhere and run it.
