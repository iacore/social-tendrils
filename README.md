# A Particular Class of Internet Tendrils

> Pierce through bubbles.

> **Warning**  
> High inbound traffic may cause permanent damage to your psyche. Ingest with care.

A frictionless interface with netizens and more.

The plan is for it support the following platforms:

- [ ] Fediverse client
	- Firefish -> See `gui/`
	- [ ] Pleroma/Akkoma
- [ ] Listiverse
	- [ ] [lotide](https://git.sr.ht/~vpzom/lotide/) (Rust)
- [ ] Matrix client (this one is easy: use existing code)
