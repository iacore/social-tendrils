const std = @import("std");

pub fn part_json() !void {
    var gpa = std.heap.GeneralPurposeAllocator(.{}){};
    defer _ = gpa.deinit();
    const a = gpa.allocator();

    const f = try std.fs.cwd().openFile("api.json", .{});
    defer f.close();

    var reader = std.json.reader(a, f.reader());
    defer reader.deinit();
    const parsed = try std.json.parseFromTokenSource(std.json.Value, a, &reader, .{});
    defer parsed.deinit();
    const value = parsed.value;
    value.dump();
}

// pub fn main() !void {
//     var gpa = std.heap.GeneralPurposeAllocator(.{}){};
//     defer _ = gpa.deinit();
//     const a = gpa.allocator();

//     var ast = try std.zig.Ast.parse(a,
//         \\const std =    @import("std");
//     , .zig);
//     defer ast.deinit(a);
// }
