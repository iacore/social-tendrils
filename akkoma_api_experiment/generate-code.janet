(import spork/json)
(def root (->> (slurp "api.json") (json/decode)))
(def paths (root "paths"))
(each x (sorted (keys paths))
      (if-not (string/find "/admin/" x)
        (print x)))

(comment
 {"paths"
  {path
    {method
      {"parameters" [{"description" _ "name" _ "schema" _}]
       "requestBody" {"content" {mimetype {"schema" _}}}
       "responses" {httpcode {"content" {mimetype {"schema" _}}
                              "description" _}}}}}})
