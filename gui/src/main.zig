const std = @import("std");
// const linenoise = @import("linenoise");
const ig = @import("zgui");
const gc = @import("gc");

const c = @cImport({
    @cInclude("../lib/imguibodge.h");
});

const Config = @import("Config.zig");
const firefish = @import("firefish.zig");
const ui = @import("ui/mod.zig");

test {
    std.testing.refAllDeclsRecursive(@This());
}

// application-wide data

pub var gpa: std.heap.GeneralPurposeAllocator(.{ .stack_trace_frames = 16 }) = .{};
const global_allocator = gpa.allocator();

pub var assets_dir: std.fs.Dir = undefined;

fn openConfigDirectory() !std.fs.Dir {
    var _buf_config_dir_path: [std.fs.MAX_PATH_BYTES]u8 = undefined;
    // todo: support XDG_HOME and XDG_CONFIG
    const home = std.os.getenv("HOME") orelse return error.EnvNoHOME;
    const path = try std.fmt.bufPrint(&_buf_config_dir_path, "{s}/.config/tendrils", .{home});
    std.fs.makeDirAbsolute(path) catch |err| {
        if (err != error.PathAlreadyExists) return err;
    };
    return try std.fs.openDirAbsolute(path, .{});
}

fn getGlobalAssetsDir() !std.fs.Dir {
    const p = try std.fs.selfExeDirPathAlloc(global_allocator); // this should be xxxxx/usr/bin (AppImage)
    defer global_allocator.free(p);
    const d0 = try std.fs.openDirAbsolute(p, .{});
    const d1 = try d0.openDir("../share/tendrils/assets", .{});
    return d1;
}

pub fn main() !void {
    // application-wide setup/teardown
    defer {
        const check = gpa.deinit();
        if (check == .leak) {}
    }

    gc.c.GC_init();
    defer gc.c.GC_deinit();

    try ui.init(global_allocator);
    defer ui.deinit();

    if (std.fs.cwd().openDir("assets", .{})) |dir| {
        assets_dir = dir;
    } else |err0| if (getGlobalAssetsDir()) |dir| {
        assets_dir = dir;
    } else |err1| {
        std.log.err("{} {}", .{ err0, err1 });
        return error.NoAssetsDir;
    }

    var config_dir = try openConfigDirectory();
    defer config_dir.close();
    // linenoise.enableUtf8();
    ig.initNoContext(global_allocator);
    defer ig.deinitNoContext();

    const file = try config_dir.openFile("config.toml", .{});
    const parsed_config = try Config.parseFromFile(global_allocator, file);
    file.close();
    defer parsed_config.deinit();
    const config_root = parsed_config.value;

    // imgui
    var _buf_assest_path = [_]u8{0} ** std.fs.MAX_PATH_BYTES;
    const assets_dir_path = try assets_dir.realpathZ(".", &_buf_assest_path);
    std.log.info("assest dir: {s}", .{assets_dir_path});
    var _buf_inifilename = [_]u8{0} ** std.fs.MAX_PATH_BYTES;
    const imgui_ini_path = try config_dir.realpathZ("imgui.ini", &_buf_inifilename);
    const _ret_init = c.imguibodge_init(.{
        .AssetsDirPath = assets_dir_path.ptr,
        .IniFilename = imgui_ini_path.ptr,
    });
    if (_ret_init != 0)
        return error.ImguiBodgeInitFailed;
    defer c.imguibodge_cleanup();

    var root = try ui.windows.Root.init(global_allocator, config_root);
    defer root.deinit();

    while (true) {
        if (c.imguibodge_poll_events()) break;
        c.imguibodge_start_frame();
        defer c.imguibodge_end_frame();
        try root.draw();
    }
}
