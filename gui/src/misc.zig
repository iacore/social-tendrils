const std = @import("std");
const gc = @import("gc");

/// Basic Future. Does not care about data leaking.
pub fn Future(comptime T: type) type {
    return struct {
        data: T = undefined,
        done: std.Thread.ResetEvent = .{},
        progress: std.Thread.ResetEvent = .{},

        pub fn stage(this: *@This()) void {
            this.progress.set();
        }

        pub fn set(this: *@This(), data: T) void {
            this.data = data;
            this.done.set();
            this.progress.reset();
        }

        pub fn getOrNull(this: @This()) ?T {
            if (this.done.isSet()) {
                return this.data;
            } else {
                return null;
            }
        }

        pub fn reset(this: *@This()) void {
            this.* = .{};
        }
    };
}

/// Print source location
pub fn todo(comptime loc: std.builtin.SourceLocation) void {
    const stderr = std.io.getStdErr().writer();
    stderr.print("todo in {s} / {s}:{}:{}\n", .{ loc.fn_name, loc.file, loc.line, loc.column }) catch {};
}

/// @return temp filename (allocated by `alloc`)
pub fn dumpFile(alloc: std.mem.Allocator, content: []const u8, prefix: []const u8, suffix: []const u8) ![]const u8 {
    var dir_tmp = try std.fs.openDirAbsolute("/tmp", .{});
    defer dir_tmp.close();
    try dir_tmp.makeDir("tendrils");
    var dir_tmp_ours = try dir_tmp.openDir("tendrils", .{});
    defer dir_tmp_ours.close();
    const filename = try std.fmt.allocPrint(alloc, "{s}{}{s}", .{ prefix, std.time.milliTimestamp(), suffix });
    const file = try dir_tmp_ours.createFile(filename, .{});
    defer file.close();
    try file.writeAll(content);
    return filename;
}

inline fn uniqueAppend(
    comptime T: type,
    comptime field_name: []const u8,
    len: usize,
    list: []T,
    items: []const T,
) usize {
    var newlen = len;
    outer: for (items) |item| {
        for (0..newlen) |i| {
            const existing = @field(list[i], field_name);
            const new = @field(item, field_name);
            if (std.mem.eql(u8, existing, new)) {
                list[i] = item;
                continue :outer;
            }
        }
        list[newlen] = item;
        newlen += 1;
    }
    return newlen;
}

pub fn mergeStruct(comptime type_list: anytype) type {
    const T = std.builtin.Type;
    const tinfo0: T.Struct = switch (@typeInfo(type_list[0])) {
        .Struct => |case| case,
        else => @compileError("Only structs are supported"),
    };
    if (tinfo0.layout != .Auto or tinfo0.is_tuple) {
        @compileError("Only structs with auto layout are supported");
    }

    const max_len = comptime cap: {
        var fields_len: usize = tinfo0.fields.len;
        var decls_len: usize = tinfo0.decls.len;
        for (1..type_list.len) |i| {
            const tinfo: T.Struct = @typeInfo(type_list[i]).Struct;
            if (tinfo0.layout != tinfo.layout) {
                @compileError("All types must have the same layout");
            }
            fields_len += tinfo.fields.len;
            decls_len += tinfo.decls.len;
        }
        break :cap .{ .fields = fields_len, .decls = decls_len };
    };

    const proto = comptime blk: {
        var struct_fields: [max_len.fields]T.StructField = undefined;
        var decls: [max_len.decls]T.Declaration = undefined;
        var flen: usize = 0;
        var dlen: usize = 0;

        for (type_list) |t| {
            const ti: T.Struct = @typeInfo(t).Struct;
            flen = uniqueAppend(T.StructField, "name", flen, &struct_fields, ti.fields);
            // TODO: Blocked by https://github.com/ziglang/zig/issues/6709
            // dlen = unique_append(T.Declaration, "name", dlen, &decls, ti.decls);
        }

        break :blk T.Struct{
            .fields = struct_fields[0..flen],
            .decls = decls[0..dlen],
            .layout = .Auto,
            .is_tuple = false,
        };
    };

    return @Type(.{ .Struct = proto });
}

fn TestMergeStruct(comptime T: type) type {
    const Base = struct { always: u8, often: T };
    const Extended = switch (T) {
        u8 => struct {
            sometimes: usize = 2,
        },
        else => struct { often: ?T = null },
    };

    return mergeStruct(.{ Base, Extended });
}
test "std.meta.merge" {
    const S1 = TestMergeStruct(u8);
    const s1 = S1{ .always = 1, .often = 2 };

    try std.testing.expect(s1.always == 1);
    try std.testing.expect(s1.often == 2);
    try std.testing.expect(s1.sometimes == 2);
    try std.testing.expect(@TypeOf(s1.always) == u8);
    try std.testing.expect(@TypeOf(s1.often) == u8);
    try std.testing.expect(@TypeOf(s1.sometimes) == usize);
    try std.testing.expect(std.meta.fields(@TypeOf(s1)).len == 3);

    const S2 = TestMergeStruct(bool);
    const s2 = S2{
        .always = 1,
        .often = true,
    };

    try std.testing.expect(s2.always == 1);
    try std.testing.expect(s2.often == true);
    try std.testing.expect(@TypeOf(s2.always) == u8);
    try std.testing.expect(@TypeOf(s2.often) == ?bool);
    try std.testing.expect(std.meta.fields(@TypeOf(s2)).len == 2);
}
