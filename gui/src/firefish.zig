//! Firefish API Client

const std = @import("std");
const ig = @import("zgui");
const gc = @import("gc");

const Future = @import("misc.zig").Future;

pub const InitOptions = struct {
    alloc: std.mem.Allocator,
    /// Base URL of the API. No trailing slash!
    /// Example: https://snug.moe/api
    apibase: []const u8,
    apikey: []const u8,
};

pub const HttpFetchError = @typeInfo(@typeInfo(@TypeOf(std.http.Client.fetch)).Fn.return_type.?).ErrorUnion.error_set;
pub const JsonParseError = std.json.ParseError(std.json.Scanner);
pub const APIError = error{
    Firefish_ClientError,
    Firefish_AuthenticationError,
    Firefish_ForbiddenError,
    Firefish_BeLikeCalc,
    Firefish_InternalServerError,
    Firefish_UnexpectedResponseCode,
};
pub const FetchImageError = std.mem.Allocator.Error || HttpFetchError || error{Firefish_UnexpectedResponseCode};
/// Errors which are considered recoverable: API errors + network errors
pub const SendRequestError = std.mem.Allocator.Error || HttpFetchError || APIError || JsonParseError;

/// API route
pub const Route = struct {
    /// endpoint url like /i
    url: []const u8,
    /// uplink type (request body)
    Tup: type,
    /// downlink type (response body)
    Tdown: type,

    pub fn init(comptime url: []const u8, comptime Tup: type, comptime Tdown: type) @This() {
        return .{ .Tup = Tup, .Tdown = Tdown, .url = url };
    }
};

pub const JsonParseDiagnostics = struct {
    line: u64,
    column: u64,
    byteOffset: u64,
    body: []const u8, // gc
};

/// I spawn HTTP requests
pub const Client = struct {
    // borrowed
    alloc: std.mem.Allocator,
    url_base: []const u8,

    // owned, sorted by initialization order
    http_header_Authorization: [:0]const u8, // by parrent_alloc
    http_headers: std.http.Headers,

    pub fn resource(this: @This(), comptime route: Route) !Resource(route) {
        return try Resource(route).init(this);
    }

    pub fn initFromConfig(alloc: std.mem.Allocator, config: @import("Config.zig").Instance) !@This() {
        return init(.{
            .alloc = alloc,
            .apibase = config.apibase,
            .apikey = config.apikey,
        });
    }

    pub fn init(opts: InitOptions) !@This() {
        var headers = std.http.Headers.init(opts.alloc);
        try headers.append("Content-Type", "application/json");
        try headers.append("Accept", "application/json");
        const bearer = try std.fmt.allocPrintZ(opts.alloc, "Bearer {s}", .{opts.apikey});
        try headers.append("Authorization", bearer);

        const url_base = opts.apibase;
        std.debug.assert(url_base[url_base.len - 1] != '/');

        return .{
            .alloc = opts.alloc,
            .url_base = url_base,

            .http_header_Authorization = bearer,
            .http_headers = headers,
        };
    }
    pub fn deinit(this: @This()) void {
        this.alloc.free(this.http_header_Authorization);
        var headers = this.http_headers;
        headers.deinit();
    }

    // pub fn sendRequestAsync(this: @This(), comptime route: Route, input: route.Tup) !*Future(SendRequestError!route.Tdown) {
    //     var fut = try gc.ALLOCATOR.create(Future(SendRequestError!route.Tdown));
    //     fut.* = .{}; // init
    //     const thread = try std.Thread.spawn(.{}, this.sendRequestAsyncHelper, .{ this, this.route, input, fut });
    //     thread.detach();
    //     return fut;
    // }

    // fn sendRequestAsyncHelper(this: @This(), comptime route: Route, input: route.Tup, fut: *Future(SendRequestError!route.Tdown)) void {
    //     fut.set(this.sendRequest(route, input));
    // }

    /// the response is allocated by libgc
    pub fn sendRequest(this: @This(), comptime route: Route, input: route.Tup, diagnostics: *JsonParseDiagnostics) SendRequestError!route.Tdown {
        comptime {
            std.debug.assert(route.url[0] != '/');
        }

        var client = std.http.Client{ .allocator = this.alloc };
        defer client.deinit();

        const url = try std.fmt.allocPrintZ(this.alloc, "{s}/{s}", .{ this.url_base, route.url });
        defer this.alloc.free(url);

        const upload_content = try std.json.stringifyAlloc(this.alloc, input, .{ .emit_null_optional_fields = false });
        defer this.alloc.free(upload_content);

        var result = try client.fetch(this.alloc, .{
            .method = .POST,
            .location = .{ .url = url },
            .headers = this.http_headers,
            .payload = .{ .string = upload_content },
        });
        defer result.deinit();
        // todo: add timeout
        // // abort if slower than 30 bytes/sec during 5 seconds
        // try client.setopt(curl.c.CURLOPT_LOW_SPEED_LIMIT, 30);
        // try client.setopt(curl.c.CURLOPT_LOW_SPEED_TIME, 5);
        // it doesn't seem like std.http supports timeout

        const body = result.body.?;

        const code: u10 = @intFromEnum(result.status);
        switch (code) {
            200 => {},
            400 => return error.Firefish_ClientError,
            401 => return error.Firefish_AuthenticationError,
            403 => return error.Firefish_ForbiddenError,
            408 => return error.Firefish_BeLikeCalc, // "I'm Calc" (don't know what this is)
            500 => return error.Firefish_InternalServerError,
            else => return error.Firefish_UnexpectedResponseCode,
        }

        return parseJsonLeaky(route, this.alloc, body, gc.ALLOCATOR, diagnostics);
    }
};

const Allocator = std.mem.Allocator;
pub fn parseJsonLeaky(comptime route: Route, scanner_alloc: Allocator, body: []const u8, out_alloc: Allocator, diagnostics: *JsonParseDiagnostics) !route.Tdown {
    var d = std.json.Diagnostics{};
    var scanner = std.json.Scanner.initCompleteInput(scanner_alloc, body);
    defer scanner.deinit();
    scanner.enableDiagnostics(&d);
    const parsed = std.json.parseFromTokenSourceLeaky(route.Tdown, out_alloc, &scanner, .{ .ignore_unknown_fields = true, .allocate = .alloc_always }) catch |err| {
        diagnostics.line = d.getLine();
        diagnostics.column = d.getColumn();
        diagnostics.byteOffset = d.getByteOffset();
        diagnostics.body = try gc.ALLOCATOR.dupe(u8, body);
        return err;
    };
    return parsed;
}

/// Request manager with retry. Manages a client, a thread, and a single API route.
/// Data of this is entirely managed by libgc
///
/// Remember to lock `response_mutex` when reading/writing `response`
pub fn Resource(comptime route: Route) type {
    return struct {
        pub const FutT = Future(SendRequestError!route.Tdown);

        // borrowed
        client: Client,

        // alloc: std.mem.Allocator,
        payload: route.Tup = .{},
        /// please hold an owned Rc(_) when reading this
        fut: *FutT,

        pub fn init(client: Client) !@This() {
            const fut = try gc.ALLOCATOR.create(FutT);
            fut.reset();
            return .{
                // .alloc = alloc,
                .client = client,
                .fut = fut,
            };
        }

        // pub fn deinit(this: @This()) void {
        //     this.alloc.destroy(this.fut);
        // }

        pub fn getResponseData(this: @This()) ?route.Tdown {
            if (this.fut.getOrNull()) |res| if (res) |data| return data else |_| {};
            return null;
        }

        pub fn reset(this: *@This()) void {
            this.fut.* = .{};
        }

        pub fn can_sendRequest(this: *@This()) bool {
            return !this.fut.progress.isSet();
        }

        pub fn sendRequest(this: *@This()) !void {
            if (!this.can_sendRequest()) @panic("please check this.can_sendRequest() before calling this");
            const thread = try std.Thread.spawn(.{}, sendRequest_inner, .{this.*});
            thread.detach();
        }
        fn sendRequest_inner(this: @This()) void {
            this.fut.stage();
            var d: JsonParseDiagnostics = undefined;
            const res = this.client.sendRequest(route, this.payload, &d);
            if (res) |_| {} else |err| {
                const a = this.client.alloc; // hack: borrow allocator
                const filename = @import("misc.zig").dumpFile(a, d.body, "json_debug-", ".json") catch "(dumpFile FAILED)";
                defer a.free(filename);
                std.log.err("JSON Parse {} at {s}:{}:{} (offset {})", .{ err, filename, d.line, d.column, d.byteOffset });
            }
            this.fut.set(res);
        }

        // ui helpers

        pub fn ui_get_inner(res: *@This()) !?route.Tdown {
            if (res.fut.getOrNull()) |result| {
                if (result) |data| {
                    return data;
                } else |err| {
                    if (errSetContains(SendRequestError, err)) {
                        ig.text("Error: {s}", .{@errorName(err)});
                    } else return err;
                }
            } else {
                if (res.fut.progress.isSet()) {
                    ig.text("Loading", .{});
                } else {
                    ig.text("Not loaded", .{});
                }
            }
            return null;
        }

        pub fn ui_button_send(res: *@This(), text: [:0]const u8) !void {
            ig.beginDisabled(.{ .disabled = !res.can_sendRequest() });
            if (ig.button(text, .{})) {
                try res.sendRequest();
            }
            ig.endDisabled();
        }
    };
}

pub fn errSetContains(comptime T: type, err: anyerror) bool {
    inline for (@typeInfo(T).ErrorSet.?) |potential_err| {
        if (std.mem.eql(u8, @errorName(err), potential_err.name)) {
            return true;
        }
    }
    return false;
}

pub const types = struct {
    const mergeStruct = @import("misc.zig").mergeStruct;

    pub const Timestamp = @import("Timestamp.zig");
    pub const string = []const u8;

    pub const UserLite = struct {
        id: string,
        name: string,
        username: string,
        host: ?string,
        avatarUrl: string,
    };

    pub const mixin_UserDetailedOnly = struct {
        url: ?string,
        uri: ?string,
        /// User Bio
        description: ?string,
        fields: []struct {
            name: string,
            value: string,
        },
        /// @example ja-JP
        lang: ?string,

        //   /** Format: uri */
        //   movedToUri: string | null;
        //   /** Format: uri */
        //   alsoKnownAs: unknown[] | null;
        //   /** Format: date-time */
        //   createdAt: string;
        //   /** Format: date-time */
        //   updatedAt: string | null;
        //   /** Format: date-time */
        //   lastFetchedAt: string | null;
        //   /** Format: url */
        //   bannerUrl: string | null;
        //   bannerBlurhash: Record<string, unknown> | null;
        //   /** @default null */
        //   bannerColor: Record<string, unknown> | null;
        //   isLocked: boolean;
        //   isSilenced: boolean;
        //   /** @example false */
        //   isSuspended: boolean;

        //   location: string | null;
        //   /** @example 2018-03-12 */
        //   birthday: string | null;
        //   followersCount: number;
        //   followingCount: number;
        //   notesCount: number;
        //   pinnedNoteIds: string[];
        //   pinnedNotes: components["schemas"]["Note"][];
        //   pinnedPageId: string | null;
        //   pinnedPage: components["schemas"]["Page"];
        //   publicReactions: boolean;

        //   isFollowing?: boolean;
        //   isFollowed?: boolean;
        //   hasPendingFollowRequestFromYou?: boolean;
        //   hasPendingFollowRequestToYou?: boolean;
        //   isBlocking?: boolean;
        //   isBlocked?: boolean;
        //   isMuted?: boolean;
        //   isRenoteMuted?: boolean;
    };

    pub const UserDetailed = mergeStruct(.{ UserLite, mixin_UserDetailedOnly });

    // const mixin_UserMeOnly = struct {
    //     // email: ?string = null,
    // };

    // pub const UserMe = mergeStruct(.{ UserDetailed, mixin_UserMeOnly });

    // this for now, since I don't need the advanced features
    pub const UserMe = UserDetailed;

    // "default":"public"
    pub const NoteVisibility = enum { public, home, followers, specified, hidden };
    pub const Note = struct {
        id: string,
        createdAt: Timestamp,
        // userId: str,
        user: *types.UserLite,
        text: ?string,
        cw: ?string,
        visibility: NoteVisibility,
        reactions: std.json.ArrayHashMap(u32),
        files: []*DriveFile,
        emojis: []*Emoji,
        // replyId: ?str,
        reply: ?*Note = null,
        // renoteId: ?str,
        /// the boosted note. `this` note is the "boost" event, with no content
        renote: ?*Note = null,
        /// I think this is the activitypub location (URI)
        uri: ?string = null,
        /// I think this is the HTTP URL (web interface)
        url: ?string = null,
    };

    pub const DriveFile = struct {
        id: string,
        createdAt: Timestamp,
        name: string,
        type: string, // mime
        isSensitive: bool,
        blurhash: string,
        url: string,
        comment: ?string,
        properties: struct {
            width: ?u32 = null,
            height: ?u32 = null,
            orientation: ?u8 = null,
            avgColor: ?string = null,
        },
    };

    pub const Emoji = struct {
        name: string,
        url: ?string,
        width: u64,
        height: u64,
    };

    /// request body
    pub const PublicTimelineRequest = struct {
        /// Only show notes that have attached files.
        /// Default: false
        withFiles: ?bool = null,
        /// integer [ 1 .. 100 ]
        /// Default 10
        limit: ?usize = null,
        /// string <misskey:id>
        sinceId: ?string = null,
        /// string <misskey:id>
        untilId: ?string = null,
        sinceDate: ?u64 = null,
        untilDate: ?u64 = null,
        /// Show replies in the timeline
        /// Default: false
        withReplies: ?bool = null,
    };
    /// request body
    pub const AuthenticatedTimelineRequest = struct {
        withFiles: ?bool = null,
        limit: ?usize = null,
        sinceId: ?string = null,
        untilId: ?string = null,
        sinceDate: ?u64 = null,
        untilDate: ?u64 = null,
        withReplies: ?bool = null,

        // Default: true
        includeMyRenotes: ?bool = null,
        // Default: true
        includeRenotedMyNotes: ?bool = null,
        // Default: true
        includeLocalRenotes: ?bool = null,
    };
};

pub const routes = struct {
    pub const whoami = Route.init("i", struct {}, *types.UserMe);
    pub const notes = struct {
        pub const global_timeline = Route.init("notes/global-timeline", types.PublicTimelineRequest, []*types.Note);
        pub const hybrid_timeline = Route.init("notes/hybrid-timeline", types.AuthenticatedTimelineRequest, []*types.Note);
    };
};

pub const Timeline = struct {
    const Item = *types.Note;
    const Tcontent = std.ArrayList(Item);

    // todo: I really need a key-value database to store this
    /// posts. smaller id (older) first
    /// each item has a ref count
    content: Tcontent,

    pub fn init(alloc: std.mem.Allocator) @This() {
        return .{
            .content = Tcontent.init(alloc),
        };
    }
    pub fn deinit(this: @This()) void {
        this.content.deinit();
    }

    pub const InsertHint = struct {
        id_before: ?[]const u8 = null,
        id_after: ?[]const u8 = null,
    };

    fn post_lessthan(_: void, lhs: Item, rhs: Item) bool {
        return std.mem.lessThan(u8, lhs.id, rhs.id);
    }

    ///
    /// # Params
    /// posts   borrowed, id sorted desc
    pub fn insertPosts(this: *@This(), posts: []Item, hint: InsertHint) !void {
        // sort posts, smaller id (older) first
        std.sort.heap(Item, posts, void{}, post_lessthan);
        if (this.content.items.len == 0) {
            for (posts) |post| {
                try this.content.append(post);
            }
        } else if (hint.id_after == null and hint.id_after != null and std.mem.eql(u8, this.content.items[0].id, hint.id_before.?)) {
            //
        } else if (hint.id_before == null and hint.id_before != null and std.mem.eql(u8, this.content.getLast().id, hint.id_after.?)) {
            //
        } else {
            // todo: allow the timeline to consist of multiple contiguous parts
            unreachable;
        }
    }

    pub fn draw(this: *@This()) void {
        for (this.content.items) |post| {
            ig.text("{s} by {s}", .{
                post.id,
                post.user.name,
            });
            if (post.text) |text|
                ig.text("{s}", .{text});
        }
    }
};

test {
    _ = tests;
}

const tests = struct {
    const t = std.testing;

    test "json can serialize null" {
        var a = std.ArrayList(u8).init(t.allocator);
        defer a.deinit();

        try std.json.stringify(null, .{}, a.writer());
        try t.expectEqualStrings("null", a.items);
    }

    test "json can serialize {}" {
        var a = std.ArrayList(u8).init(t.allocator);
        defer a.deinit();

        try std.json.stringify(@as(struct {}, .{}), .{}, a.writer());
        try t.expectEqualStrings("{}", a.items);
    }

    test "concrete test 1" {
        const filename = "testset/tendrils-dump-1694211662423.json";
        const file = try std.fs.cwd().openFile(filename, .{});
        defer file.close();
        const unparsed = try file.reader().readAllAlloc(t.allocator, std.math.maxInt(usize));
        defer t.allocator.free(unparsed);
        var arena = std.heap.ArenaAllocator.init(t.allocator);
        defer arena.deinit();
        const out_alloc = arena.allocator();
        var d: JsonParseDiagnostics = undefined;

        const value = parseJsonLeaky(routes.notes.global_timeline, t.allocator, unparsed, out_alloc, &d) catch |err| {
            std.log.err("{s}:{}:{}", .{ filename, d.line, d.column });
            return err;
        };
        _ = value;
    }
};

// `alloc`: allocator must be multi-threaded
pub fn fetchImageAsync(alloc: std.mem.Allocator, url: [:0]const u8, fut: *Future(FetchImageError![]const u8)) !void {
    const thread = try std.Thread.spawn(.{}, fetchImage_inner, .{ alloc, url, fut });
    thread.detach();
}
fn fetchImage_inner(alloc: std.mem.Allocator, url: [:0]const u8, fut: *Future(FetchImageError![]const u8)) void {
    fut.stage();
    fut.set(fetchImage(alloc, url));
}

/// @return gc-managed data
pub fn fetchImage(alloc: std.mem.Allocator, url: [:0]const u8) FetchImageError![]const u8 {
    var client = std.http.Client{ .allocator = alloc };
    defer client.deinit();

    var result = try client.fetch(gc.ALLOCATOR, .{
        .method = .GET,
        .location = .{ .url = url },
        // todo: add timeout
        // todo: default max response size is 16MiB. maybe not enough for image?
    });
    errdefer result.deinit(); // no deinit because data is returned with GC

    const body = result.body.?;

    const code: u10 = @intFromEnum(result.status);
    switch (code) {
        200 => {},
        else => {
            std.log.err("Unexpected response code when fetching image.\nurl:{s}\ncode: {}", .{ url, code });
            return error.Firefish_UnexpectedResponseCode;
        },
    }
    return body;
}
