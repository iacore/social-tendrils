pub const widgets = @import("widgets.zig");
pub const windows = @import("windows.zig");

// deps
const std = @import("std");
const ui = @import("zgui");

pub const c = @cImport({
    @cInclude("GL/gl.h");
    @cInclude("SDL2/SDL_image.h");
});

pub const Texture = c.GLuint;

pub fn init(alloc: std.mem.Allocator) !void {
    try widgets.init(alloc);
}
pub fn deinit() void {
    widgets.deinit();
}

pub fn createTexture() Texture {
    var texture: Texture = undefined;

    c.glGenTextures(1, &texture);
    c.glBindTexture(c.GL_TEXTURE_2D, texture);

    // Setup filtering parameters for display
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MIN_FILTER, c.GL_LINEAR);
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_MAG_FILTER, c.GL_LINEAR);
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_S, c.GL_CLAMP_TO_EDGE); // This is required on WebGL for non power-of-two textures
    c.glTexParameteri(c.GL_TEXTURE_2D, c.GL_TEXTURE_WRAP_T, c.GL_CLAMP_TO_EDGE); // Same

    return texture;
}

pub fn loadTexture(tex: Texture, surface: c.SDL_Surface) void {
    c.glBindTexture(c.GL_TEXTURE_2D, tex);
    c.glTexImage2D(c.GL_TEXTURE_2D, 0, 3, surface.w, surface.h, 0, c.GL_RGB, c.GL_UNSIGNED_BYTE, surface.pixels);
}

pub fn deleteTexture(tex: Texture) void {
    c.glDeleteTextures(1, &tex);
}
