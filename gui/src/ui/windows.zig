const std = @import("std");
const main = @import("../main.zig");
const ig = @import("zgui");
const Config = @import("../Config.zig");
const firefish = @import("../firefish.zig");
const widgets = @import("widgets.zig");
const routes = firefish.routes;

const todo = @import("../misc.zig").todo;
const Arena = std.heap.ArenaAllocator;

const ui = @import("mod.zig");
const c = ui.c;
const Texture = ui.Texture;

pub const PerInstanceBundle = struct {
    client: firefish.Client,
    timeline: TimelineTest,
    user: UserProfile,
    // meta: InstanceMeta,
};

pub const Root = struct {
    arena: *Arena,
    config: Config,

    windows: []PerInstanceBundle,

    show_demo_window: bool = false,
    buffer_unicode_test: [128]u8 = "一隻🦆 a 🅰️".* ++ [_]u8{0} ** 108,
    demo_texture_id: Texture,

    pub fn init(alloc: std.mem.Allocator, config: Config) !@This() {
        var _buf_demo_icon_path = [_]u8{0} ** std.fs.MAX_PATH_BYTES;
        const demo_icon_path = try main.assets_dir.realpath("placeholder.png", &_buf_demo_icon_path);
        const maybe_surface = c.IMG_Load(demo_icon_path.ptr);
        if (maybe_surface == null) unreachable; //todo
        defer c.SDL_FreeSurface(maybe_surface);

        const surface = maybe_surface.*;
        const texture = ui.createTexture();
        ui.loadTexture(texture, surface);

        const arena = try alloc.create(Arena);
        arena.* = Arena.init(alloc);
        const a = arena.allocator();

        var bundles = try a.alloc(PerInstanceBundle, config.instance.len);
        for (bundles, config.instance) |*bundle, instance_config| {
            const client = try firefish.Client.initFromConfig(a, instance_config);
            bundle.client = client;
            bundle.timeline = try TimelineTest.init(a, instance_config, client);
            bundle.user = try UserProfile.init(instance_config, client);
        }
        return .{
            .arena = arena,
            .config = config,
            .windows = bundles,
            .demo_texture_id = texture,
        };
    }
    pub fn deinit(this: @This()) void {
        ui.deleteTexture(this.demo_texture_id);

        for (this.windows) |bundle| bundle.client.deinit();
        this.arena.deinit();
        this.arena.child_allocator.destroy(this.arena);
    }

    pub fn draw(this: *@This()) !void {
        if (ig.begin("Instances", .{
            .flags = .{
                .always_auto_resize = true,
            },
        })) {
            if (ig.beginTable("instance table", .{ .column = 3 })) {
                ig.tableSetupColumn("name", .{});
                ig.tableSetupColumn("backend", .{});
                ig.tableSetupColumn("Actions", .{});
                ig.tableHeadersRow();

                for (this.config.instance, 0..) |instance, i| {
                    _ = ig.tableNextColumn();
                    ig.text("{s}", .{instance.name});
                    _ = ig.tableNextColumn();
                    ig.text("{s}", .{@tagName(instance.type)});
                    _ = ig.tableNextColumn();
                    if (ig.smallButton("Instance")) {
                        todo(@src());
                    }
                    ig.sameLine(.{});
                    if (ig.smallButton("User")) {
                        this.windows[i].user.show = true;
                    }
                    ig.sameLine(.{});
                    if (ig.smallButton("Timeline")) {
                        this.windows[i].timeline.show = true;
                    }
                }
            }
            ig.endTable();

            ig.separator();

            // demo section begin

            _ = ig.checkbox("Show Demo Window", .{ .v = &this.show_demo_window });
            _ = ig.inputText("emoji test", .{ .buf = &this.buffer_unicode_test });

            ig.image(@ptrFromInt(this.demo_texture_id), .{ .w = 64, .h = 64 });
            ig.sameLine(.{});
            ig.text("<- should show a checkerboard image", .{});
        }
        ig.end();

        for (this.windows) |*bundle| {
            try bundle.timeline.draw();
            try bundle.user.draw();
        }

        if (this.show_demo_window) ig.showDemoWindow(&this.show_demo_window);
    }
};

pub const TimelineTest = struct {
    // borrowed
    alloc: std.mem.Allocator,
    config: Config.Instance,
    client: firefish.Client,

    show: bool = true, // todo: save this field
    res_timeline_hybrid: firefish.Resource(routes.notes.hybrid_timeline),
    res_timeline_public: firefish.Resource(routes.notes.global_timeline),
    timeline: firefish.Timeline,

    pub fn init(alloc: std.mem.Allocator, config: Config.Instance, client: firefish.Client) !@This() {
        var this: @This() = undefined;
        this = .{
            .alloc = alloc,
            .config = config,
            .client = client,
            .res_timeline_hybrid = try client.resource(routes.notes.hybrid_timeline),
            .res_timeline_public = try client.resource(routes.notes.global_timeline),
            .timeline = firefish.Timeline.init(alloc),
        };
        return this;
    }
    pub fn deinit(this: @This()) void {
        this.timeline.deinit();
    }

    pub fn draw(this: *@This()) !void {
        const window_name = ig.formatZ(@typeName(TimelineTest) ++ " {s}", .{this.config.name});

        if (this.show) {
            defer ig.end();
            if (ig.begin(window_name, .{ .popen = &this.show })) {
                try this.res_timeline_public.ui_button_send("Fetch Public Timeline");
                if (try this.res_timeline_public.ui_get_inner()) |timeline| {
                    try this.timeline.insertPosts(timeline, .{});
                    this.res_timeline_public.reset();
                }

                try this.res_timeline_hybrid.ui_button_send("Fetch Hybrid Timeline");
                if (try this.res_timeline_hybrid.ui_get_inner()) |timeline| {
                    try this.timeline.insertPosts(timeline, .{});
                    this.res_timeline_hybrid.reset();
                }

                this.timeline.draw();
            }
        }
    }
};

pub const UserProfile = struct {
    show: bool = true,
    res: firefish.Resource(routes.whoami),
    config: Config.Instance,

    pub fn init(config: Config.Instance, client: firefish.Client) !@This() {
        var res = try client.resource(routes.whoami);
        return .{
            .config = config,
            .res = res,
        };
    }

    pub fn draw(this: *@This()) !void {
        const window_name = ig.formatZ("User Profile {s}", .{this.config.name});
        if (this.show) {
            defer ig.end();
            if (ig.begin(window_name, .{ .popen = &this.show })) {
                try this.res.ui_button_send("Refresh");

                if (try this.res.ui_get_inner()) |user| {
                    // bug?: str_id is unchanged. maybe this is a imgui bug
                    try widgets.image("avatar", user.avatarUrl, .{ .w = 64, .h = 64 });
                    ig.sameLine(.{});
                    {
                        ig.beginGroup();
                        defer ig.endGroup();
                        ig.text("{s}", .{user.name});
                        if (user.host) |host| {
                            ig.text("@{s}@{s} ({s})", .{ host, user.username, user.id });
                        } else {
                            ig.text("(local)@{s} ({s})", .{ user.username, user.id });
                        }
                        if (user.lang) |lang| {
                            ig.text("Language: {s}", .{lang});
                        }
                    }

                    if (user.description) |desc| {
                        if (ig.treeNodeFlags("Bio", .{ .default_open = true })) {
                            defer ig.treePop();
                            ig.textWrapped("{s}", .{desc});
                        }
                    }
                    if (ig.treeNodeFlags("Fields", .{ .default_open = true })) {
                        defer ig.treePop();
                        for (user.fields) |field| {
                            ig.text("{s}", .{field.name});
                            ig.sameLine(.{});
                            ig.text("{s}", .{field.value});
                        }
                    }
                }
            }
        }
    }
};
