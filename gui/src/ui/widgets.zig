const std = @import("std");
const ig = @import("zgui");
const gc = @import("gc");
const firefish = @import("../firefish.zig");

const Future = @import("../misc.zig").Future;
const FetchError = firefish.FetchImageError;

const ui = @import("mod.zig");
const c = ui.c;
const Texture = ui.Texture;

// global section start

var alloc: std.mem.Allocator = undefined;
/// the keys are GC-allocated
/// the values are allecated by `alloc` (see above)
var imageCache: std.StringHashMap(CachedImage) = undefined;

pub fn init(_alloc: std.mem.Allocator) !void {
    alloc = _alloc;
    imageCache = std.StringHashMap(CachedImage).init(_alloc);
}

pub fn deinit() void {
    defer imageCache.deinit();
    var it = imageCache.iterator();
    while (it.next()) |item| {
        item.value_ptr.free_noCheck();
    }
}

// global section end

const CachedImage = struct {
    pub const FutT = Future(FetchError![]const u8);

    allocator: std.mem.Allocator,
    texture: Texture,
    state: enum { pending, loaded, failed } = .pending,
    // the slice is allocated by gc, probably
    fut: *FutT,

    pub fn init(allocator: std.mem.Allocator, texture: Texture) !@This() {
        const fut = try allocator.create(FutT);
        fut.reset();
        return .{ .allocator = allocator, .texture = texture, .fut = fut };
    }
    pub fn deinit(this: @This()) void {
        // important invariant
        // if CachedImage.state == .pending, it shouldn't be removed. otherwise possible race condition
        if (this.state == .pending) @panic("race condition");
        this.free_noCheck();
    }
    fn free_noCheck(this: @This()) void {
        this.allocator.destroy(this.fut);
        ui.deleteTexture(this.texture);
    }
};

pub const ImageOptions = struct {
    w: f32,
    h: f32,
};

// maybe the better aproach is to add a button for the user to clean up image cache
pub fn collectUnusedImage() void {
    //todo
    // delete key
    // delete texture
    // delete fut
}

// todo: some errors here are not fatal. handle those, and do not throw
pub fn image(str_id: [:0]const u8, url: []const u8, opts: ImageOptions) !void {
    collectUnusedImage();

    const item = try imageCache.getOrPut(url);
    const img: *CachedImage = item.value_ptr;

    if (item.found_existing) {
        load_image: {
            if (img.state == .pending and img.fut.done.isSet()) {
                const data = img.fut.data catch |err| {
                    std.log.err("unable to download image, {}", .{err});
                    img.state = .failed;
                    break :load_image;
                };

                const rw = c.SDL_RWFromConstMem(@ptrCast(data.ptr), @intCast(data.len));
                const surface = c.IMG_Load_RW(rw, 1);
                if (surface == null) {
                    std.log.err("unable to load image from memory, url: {s}", .{url});
                    img.state = .failed;
                    break :load_image;
                }
                ui.loadTexture(img.texture, surface.*);
                img.state = .loaded;
            }
        }
        if (img.state == .failed) {
            // todo: load "failed" image to texture
        }
    } else {
        const url_duped = try gc.ALLOCATOR.dupeZ(u8, url);
        item.key_ptr.* = url_duped;
        img.* = try CachedImage.init(alloc, ui.createTexture());
        try firefish.fetchImageAsync(alloc, url_duped, img.fut);
    }

    if (ig.imageButton(str_id, @ptrFromInt(img.texture), .{
        .w = opts.w,
        .h = opts.h,
    })) {
        if (img.state == .failed) {
            // retry again
            img.deinit();
            defer alloc.free(item.key_ptr.*);
            imageCache.removeByPtr(item.key_ptr);
        } else if (img.state == .loaded) {
            // todo: open in external editor
        }
    }
}
