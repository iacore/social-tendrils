const std = @import("std");
const lmdb = @import("lmdb");
const s2s = @import("s2s");

var gpa = std.heap.GeneralPurposeAllocator(.{}){};
const alloc = gpa.allocator();

const Post = struct {
    id: []const u8,
    visibility: []const u8,
    content: []const u8,
};

pub fn main() !void {
    defer _ = gpa.deinit();

    var path = "/tmp/lmdbtest";

    const env = try lmdb.Environment.init(path, .{ .max_num_dbs = 1 });
    defer env.deinit();

    const sample_post = .{
        .id = "id here",
        .visibility = "public",
        .content = "do i need this",
    };
    {
        const tx = try env.begin(.{});
        errdefer tx.deinit();

        const db = try tx.open("posts", .{ .allow_duplicate_keys = true, .create_if_not_exists = true });
        defer db.close(env);

        var a = std.ArrayList(u8).init(alloc);
        defer a.deinit();

        try s2s.serialize(a.writer(), Post, sample_post);

        std.log.info("key.len={}", .{a.items.len});
        try tx.put(db, sample_post.id, a.items, .{});
        try tx.commit();
    }

    std.log.info("num_entries={}", .{env.stat().num_entries});

    {
        const tx = try env.begin(.{ .read_only = true });
        defer tx.deinit();
        const db = try tx.open("posts", .{});
        defer db.close(env);
        const cursor = try tx.cursor(db);
        defer cursor.deinit();

        while (try cursor.get(.next)) |entry| {
            var stream = std.io.fixedBufferStream(entry.val);
            var post = try s2s.deserializeAlloc(stream.reader(), Post, alloc);
            defer s2s.free(alloc, Post, &post);
            std.log.info("key={s} val.content={s}", .{ entry.key, post.content });
        }
    }
}
