const std = @import("std");
const main = @import("../main.zig");
const Firefish = @import("../Firefish.zig");

const config = &main.config;
const stdout = &main.stdout;

comptime {
    const builtin = @import("builtin");
    if (builtin.optimize != .Debug) @compileError("Stuff in this file shouldn't be used in Release mode");
}

pub fn test_firefish() !void {
    // const apikey = env.get("APIKEY") orelse return error.NoAPIKEY;
    const apikey = config.instance[0].apikey;
    try stdout.print("apikey: {s}\n", .{apikey});

    var client = try Firefish.init(.{ .allocator = std.testing.allocator, .apikey = apikey, .apibase = "https://snug.moe/api" });
    defer client.deinit();

    const user = try client.whoami();
    try stdout.print("My ID: {s}\n", .{user.id});

    const user2 = try client.whoami();
    try stdout.print("My ID: {s}\n", .{user2.id});

    // var line: [*c]u8 = 0;
    // _ = line;
    // while (cond: {
    //     line = linenoise.readLine("hello> ");
    //     break :cond line != 0;
    // }) {
    //     defer linenoise.free(line);
    //     try stdout.print("You wrote: {s}\n", .{line});
    // }
}
