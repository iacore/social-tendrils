//! Global Config Struct

const std = @import("std");
const toml = @import("toml");

instance: []Instance,

pub const Instance = struct {
    /// instance type
    type: Type,
    /// user readable name
    name: []const u8,
    /// the apikey. You can get one in your settings
    apikey: []const u8,
    /// Base URL of API endpoint without trailing slash
    /// e.g. https://snug.moe/api
    apibase: []const u8,

    pub const Type = enum {
        firefish,
    };
};

/// Parse toml
pub fn parseFromSlice(alloc: std.mem.Allocator, slice: []const u8) !toml.Parsed(@This()) {
    var parser = toml.Parser(@This()).init(alloc);
    defer parser.deinit();

    var dest = parser.parseString(slice) catch |err| {
        switch (parser.error_info.?) {
            // ok error reporting
            .parse => |a| {
                std.log.err("Error parsing TOML at {}:{}", .{ a.line, a.pos });
            },
            .struct_mapping => |a| {
                std.log.err("Error parsing TOML with struct_mapping {s}", .{a});
            },
        }
        return err;
    };
    return dest;
}

pub fn parseFromFile(alloc: std.mem.Allocator, file: std.fs.File) !toml.Parsed(@This()) {
    var content = try file.readToEndAlloc(alloc, 1024 * 1024);
    defer alloc.free(content);

    return parseFromSlice(alloc, content);
}

const ConfigRoot = @This();

test {
    const example_config =
        \\[[instance]]
        \\type = "firefish"
        \\name = "AbcDFE"
        \\apikey = "AbcDFE"
        \\apibase = "AbcDFE"
    ;

    var dest = try ConfigRoot.parseFromSlice(std.testing.allocator, example_config);
    defer dest.deinit();

    try std.testing.expectEqualStrings("AbcDFE", dest.value.instance[0].apikey);
}

test {
    const example_config =
        \\[[instance]]
        \\name = "AbcDFE"
        \\apikey = "AbcDFE"
    ;

    var parser = toml.Parser(ConfigRoot).init(std.testing.allocator);

    var dest = parser.parseString(example_config);

    parser.deinit();

    try std.testing.expectError(error.MissingRequiredField, dest);
}

test {
    const example_config =
        \\[[instance]]
        \\type = "no"
        \\name = "AbcDFE"
        \\apikey = "AbcDFE"
        \\apibase = "AbcDFE"
    ;

    var parser = toml.Parser(ConfigRoot).init(std.testing.allocator);

    var dest = parser.parseString(example_config);

    parser.deinit();

    try std.testing.expectError(error.InvalidValueType, dest);
}
