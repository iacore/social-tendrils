## Project Details

`TwitterColorEmoji-SVGinOT.ttf` downloaded from [here](https://github.com/13rac1/twemoji-color-font/releases/download/v14.0.2/TwitterColorEmoji-SVGinOT-14.0.2.zip)

## Building

`zig build run`

Tested with Zig 0.11.0

### Module Naming

`gc` is bohemgc.  
`ig` is imgui. `ui` is our own UI module.

### A few problems:

Firefish API is unversioned, and I can't develop Firefish. Bugs in Firefish API cannot be fixed by me.

### Observations (for developers)

std.time.Instant.timestamp is per-second

public timeline contain every public visibility post. It's updates really fast.

hybrid timeline = "Social timeline" = Home + Local

Firefish has endpoint for streaming: wss://<HOST>/streaming

## TODO

- [ ] automatically search for system fonts
- [ ] url widget (open in browser/copy)
- [ ] user profile
- [ ] timeline view
    - load more
    - mini post view
    - open post
- [ ] indiviual post view
    - basic ui
        - open user
        - image
        - video
        - poll
    - reply
    - boost
    - star
    - react
    - quote

- send post
    - send text
    - send emoji
    - send file (video, image)
    - add caption to file

- [ ] Direct Message
- [ ] reply
	- [ ] cross-server emoji reaction
	- [ ] post stuff
		- [ ] images
		- [ ] upload files


- get a proper icon (256x256 png) for AppImage

- write a proper readme

## Packaging

`./package-appimage`: Package AppImage
