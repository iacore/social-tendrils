const std = @import("std");
const zgui = @import("lib/zig-gamedev/libs/zgui/build.zig");
const gc = @import("lib/zig-libgc/build.zig");
const LazyPath = std.Build.LazyPath;

var lib_linenoise: ?*std.Build.Step.Compile = null;
var lib_imgui: ?*std.Build.Step.Compile = null;
var lib_gc: ?*std.Build.Step.Compile = null;
var mod_gc: ?*std.Build.Module = null;
var pkg_zgui: ?zgui.Package = null;
// var dep_lmdb: *std.Build.Dependency = undefined;
// var dep_s2s: *std.Build.Dependency = undefined;

fn link_exe(b: *std.Build, exe: *std.Build.Step.Compile) void {
    exe.linkLibC();
    exe.addIncludePath(LazyPath.relative("src"));

    // exe.linkLibrary(dep_lmdb.artifact("lmdb"));
    // exe.addModule("lmdb", dep_lmdb.module("lmdb"));

    // exe.addModule("s2s", dep_s2s.module("s2s"));

    // exe.addAnonymousModule("rcsp", .{
    //     .source_file = .{ .path = "lib/zig-rcsp/src/main.zig" },
    // });

    exe.addAnonymousModule("time", .{
        .source_file = .{ .path = "lib/time.zig" },
    });

    exe.addAnonymousModule("mecha", .{
        .source_file = .{ .path = "lib/mecha/mecha.zig" },
    });

    // linenoise
    // exe.addIncludePath(LazyPath.relative("lib/linenoise-mob"));
    // if (lib_linenoise == null) {
    //     const lib = b.addStaticLibrary(.{
    //         .name = "linenoise",
    //         .target = exe.target,
    //         .optimize = .ReleaseSafe,
    //         .link_libc = true,
    //     });
    //     lib.addCSourceFile(.{ .file = LazyPath.relative("lib/linenoise-mob/linenoise.c"), .flags = &.{} });
    //     lib.addCSourceFile(.{ .file = LazyPath.relative("lib/linenoise-mob/utf8.c"), .flags = &.{} });
    //     lib_linenoise = lib;
    // }
    // exe.linkLibrary(lib_linenoise.?);
    // exe.addAnonymousModule("linenoise", .{
    //     .source_file = .{ .path = "lib/linenoise.zig" },
    // });

    if (lib_gc == null) {
        const pkg = b.anonymousDependency("lib/zig-libgc", gc, .{});
        lib_gc = pkg.artifact("gc");
        mod_gc = pkg.module("gc");
    }
    exe.linkLibrary(lib_gc.?);
    exe.addModule("gc", mod_gc.?);

    // imgui + lunasvg
    if (lib_imgui == null) {
        const lib = b.addStaticLibrary(.{
            .name = "imgui",
            .target = exe.target,
            // .optimize = .ReleaseSafe,
            .optimize = exe.optimize,
            .link_libc = true,
        });
        lib.linkLibCpp();
        lib.linkSystemLibrary("SDL2");
        exe.linkSystemLibrary("SDL2_image");
        lib.linkSystemLibrary("GL");
        lib.linkSystemLibrary("freetype2");
        // lib.addIncludePath(LazyPath.relative("lib"));
        lib.defineCMacro("IMGUI_USER_CONFIG",
            \\"../imconfig_custom.h"
        );
        lib.addIncludePath(LazyPath.relative("lib/imgui"));
        lib.addIncludePath(LazyPath.relative("lib/imgui/misc/freetype"));
        lib.addIncludePath(LazyPath.relative("lib/imgui/backends"));
        lib.addIncludePath(LazyPath.relative("lib/lunasvg/include"));
        lib.addIncludePath(LazyPath.relative("lib/lunasvg/3rdparty/plutovg"));
        lib.addIncludePath(LazyPath.relative("lib/lunasvg/3rdparty/stb"));
        lib.addCSourceFiles(&.{
            "lib/imgui/imgui.cpp",
            "lib/imgui/imgui_demo.cpp",
            "lib/imgui/imgui_draw.cpp",
            "lib/imgui/imgui_tables.cpp",
            "lib/imgui/imgui_widgets.cpp",
            "lib/imgui/backends/imgui_impl_sdl2.cpp",
            "lib/imgui/backends/imgui_impl_opengl3.cpp",
            "lib/imguibodge.cpp", //bodge

            "lib/lunasvg/3rdparty/plutovg/plutovg-blend.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-dash.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-ft-math.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-ft-raster.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-ft-stroker.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-geometry.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-paint.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg-rle.c",
            "lib/lunasvg/3rdparty/plutovg/plutovg.c",
        }, &.{});
        lib.addCSourceFiles(&.{
            "lib/imgui/misc/freetype/imgui_freetype.cpp",

            "lib/lunasvg/source/lunasvg.cpp",
            "lib/lunasvg/source/element.cpp",
            "lib/lunasvg/source/property.cpp",
            "lib/lunasvg/source/parser.cpp",
            "lib/lunasvg/source/layoutcontext.cpp",
            "lib/lunasvg/source/canvas.cpp",
            "lib/lunasvg/source/clippathelement.cpp",
            "lib/lunasvg/source/defselement.cpp",
            "lib/lunasvg/source/gelement.cpp",
            "lib/lunasvg/source/geometryelement.cpp",
            "lib/lunasvg/source/graphicselement.cpp",
            "lib/lunasvg/source/maskelement.cpp",
            "lib/lunasvg/source/markerelement.cpp",
            "lib/lunasvg/source/paintelement.cpp",
            "lib/lunasvg/source/stopelement.cpp",
            "lib/lunasvg/source/styledelement.cpp",
            "lib/lunasvg/source/styleelement.cpp",
            "lib/lunasvg/source/svgelement.cpp",
            "lib/lunasvg/source/symbolelement.cpp",
            "lib/lunasvg/source/useelement.cpp",
        }, &.{"-fno-sanitize=undefined"});
        lib_imgui = lib;
    }
    exe.linkLibrary(lib_imgui.?);

    // zgui
    if (pkg_zgui == null) {
        const pkg = zgui.package(b, exe.target, .ReleaseSafe, .{
            .options = .{
                .backend = .no_backend,
                .with_imgui = false,
                .with_implot = false,
            },
        });
        const lib = pkg.zgui_c_cpp;
        // lib.addIncludePath(LazyPath.relative("lib"));
        lib.defineCMacro("IMGUI_USER_CONFIG",
            \\"../imconfig_custom.h"
        );
        lib.addIncludePath(LazyPath.relative("lib/imgui"));

        pkg_zgui = pkg;
    }
    pkg_zgui.?.link(exe);

    // toml
    exe.addAnonymousModule("toml", .{
        .source_file = .{ .path = "lib/zig-toml/src/main.zig" },
    });
}

pub fn build(b: *std.Build) void {
    const target = b.standardTargetOptions(.{});

    const optimize = b.standardOptimizeOption(.{});

    //dep_lmdb = b.anonymousDependency("lib/lmdb-zig", @import("lib/lmdb-zig/build.zig"), .{});
    // dep_s2s = b.anonymousDependency("lib/s2s", @import("lib/s2s/build.zig"), .{});

    const exe = b.addExecutable(.{
        .name = "tendrils",

        .root_source_file = .{ .path = "src/main.zig" },
        .target = target,
        .optimize = optimize,
    });
    link_exe(b, exe);
    b.installArtifact(exe);
    { // zig build run
        const run_cmd = b.addRunArtifact(exe);

        run_cmd.step.dependOn(b.getInstallStep());

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("run", "Run the app");
        run_step.dependOn(&run_cmd.step);
    }
    { // zig build lldb
        const run_cmd = b.addSystemCommand(&.{ "lldb", "--source-quietly", "-s", "lib/lldbscript" });

        run_cmd.step.dependOn(b.getInstallStep());

        run_cmd.addArtifactArg(exe);
        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("lldb", "Debug the app with lldb");
        run_step.dependOn(&run_cmd.step);
    }

    {
        const unit_tests = b.addTest(.{
            .root_source_file = .{ .path = "src/main.zig" },
            .target = target,
            .optimize = optimize,
        });
        link_exe(b, unit_tests);

        const run_unit_tests = b.addRunArtifact(unit_tests);

        const test_step = b.step("test", "Run unit tests");
        test_step.dependOn(&run_unit_tests.step);
    }

    {
        const exe_scratch = b.addExecutable(.{
            .name = "tendrils-scratch",

            .root_source_file = .{ .path = "src/_scratch/scratch_main.zig" },
            .target = target,
            .optimize = optimize,
        });
        link_exe(b, exe_scratch);

        const run_cmd = b.addRunArtifact(exe_scratch);

        run_cmd.step.dependOn(b.getInstallStep());

        if (b.args) |args| {
            run_cmd.addArgs(args);
        }

        const run_step = b.step("scratch", "Run scratch_main");
        run_step.dependOn(&run_cmd.step);
    }
}
