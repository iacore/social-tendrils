#pragma once

#include <stdbool.h>
#include <SDL.h>
#if defined(IMGUI_IMPL_OPENGL_ES2)
#include <SDL_opengles2.h>
#else
#include <SDL_opengl.h>
#endif


typedef struct {
    char* IniFilename;
    char* AssetsDirPath;
} imguibodge_init_options;


// Our state
extern SDL_Window* window;
extern SDL_GLContext gl_context;
extern bool show_demo_window;
extern bool show_another_window;
extern bool should_close_window;

int imguibodge_init(imguibodge_init_options opts);
void imguibodge_cleanup();
bool imguibodge_poll_events();
void imguibodge_start_frame();
void imguibodge_end_frame();
int imguibodge_main();

void imguibodge_push_font(const char* ttf_path, float font_size);
