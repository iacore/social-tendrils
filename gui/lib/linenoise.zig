const c = @cImport({
    @cInclude("linenoise.h");
    @cInclude("utf8.h");
});

/// marker pointer. only compare, do not read
pub const EDIT_MORE = c.linenoiseEditMore;

pub const State = c.linenoiseState;

pub const Completions = c.linenoiseCompletions;

pub const editStart = c.linenoiseEditStart;
pub const editFeed = c.linenoiseEditFeed;
pub const editStop = c.linenoiseEditStop;
pub const hide = c.linenoiseHide;
pub const show = c.linenoiseShow;

// Blocking API.
pub const readLine = c.linenoise;
pub const free = c.linenoiseFree;

// Completion API.
pub const completionCallback = c.linenoiseCompletionCallback;
pub const hintsCallback = c.linenoiseHintsCallback;
pub const freeHintsCallback = c.linenoiseFreeHintsCallback;
pub const setCompletionCallback = c.linenoiseSetCompletionCallback;
pub const setHintsCallback = c.linenoiseSetHintsCallback;
pub const setFreeHintsCallback = c.linenoiseSetFreeHintsCallback;
pub const addCompletion = c.linenoiseAddCompletion;

// History API.
pub const historyAdd = c.linenoiseHistoryAdd;
pub const historySetMaxLen = c.linenoiseHistorySetMaxLen;
pub const historySave = c.linenoiseHistorySave;
pub const historyLoad = c.linenoiseHistoryLoad;

// Other utilities.
pub const clearScreen = c.linenoiseClearScreen;
pub const setMultiLine = c.linenoiseSetMultiLine;
pub const printKeyCodes = c.linenoisePrintKeyCodes;
pub const maskModeEnable = c.linenoiseMaskModeEnable;
pub const maskModeDisable = c.linenoiseMaskModeDisable;

// UTF-8 support.
pub const setEncodingFunctions = c.linenoiseSetEncodingFunctions;
pub const utf8PrevCharLen = c.linenoiseUtf8PrevCharLen;
pub const utf8NextCharLen = c.linenoiseUtf8NextCharLen;
pub const utf8ReadCode = c.linenoiseUtf8ReadCode;

pub fn enableUtf8() void {
    setEncodingFunctions(utf8PrevCharLen, utf8NextCharLen, utf8ReadCode);
}
